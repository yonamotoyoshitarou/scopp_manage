function nav(){
    var html = "";


    html += '    <div class="navbar-header">';
    html += '      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">';
    html += '        <span class="sr-only">Toggle navigation</span>';
    html += '        <span class="icon-bar"></span>';
    html += '        <span class="icon-bar"></span>';
    html += '        <span class="icon-bar"></span>';
    html += '      </button>';
    html += '      <a class="navbar-brand" href="/">SCOPP管理画面</a>';
    html += '    </div>';
    html += '    <div id="navbar" class="navbar-collapse collapse">';
    html += '      <ul class="nav navbar-nav navbar-right">';

//    html += '        <li><a href="#">Dashboard</a></li>';

    html += '        <li class="dropdown">';
    html += '          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">マスタ<span class="caret"></span>';
    html += '          </a>';
    html += '          <ul class="dropdown-menu" role="menu">';
    html += '            <li><a href="/master/title/">アプリ/サイト</a></li>';
    html += '            <li><a href="/master/creative/">クリエイティブ</a></li>';   //　Application内に置いたとしても、一覧はほしいか
    html += '            <li><a href="/master/client/">広告主</a></li>';
    html += '            <li><a href="/master/client-department/">広告主部署</a></li>';
    html += '            <li><a href="/master/agent/">代理店</a></li>';
    html += '            <li><a href="/master/agent-department/">代理店部署</a></li>';
    html += '            <li><a href="/master/partner/">パートナー</a></li>';
    html += '          </ul>';
    html += '        </li>';
    html += '        <li class="dropdown">';
    html += '          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">キャンペーン<span class="caret"></span>';
    html += '          </a>';
    html += '          <ul class="dropdown-menu" role="menu">';
    html += '            <li><a href="/ticket/project/">プロジェクト</a></li>';
    html += '            <li><a href="/ticket/campaign/">キャンペーン</a></li>';
    html += '          </ul>';
    html += '        </li>';
    html += '        <li class="dropdown">';
    html += '          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">レポート<span class="caret"></span>';
    html += '          </a>';
    html += '          <ul class="dropdown-menu" role="menu">';
    html += '            <li><a href="/report/project">プロジェクト</a></li>';
    html += '            <li><a href="/report/campaign">キャンペーン</a></li>';
    html += '            <li><a href="#">外部提供データ</a></li>';
    html += '            <li><a href="/report/bill">請求データ</a></li>';
    html += '          </ul>';
    html += '        </li>';
    html += '        <li class="dropdown">';
    html += '          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>';
    html += '          </a>';
    html += '          <ul class="dropdown-menu" role="menu">';
    html += '            <li><a href="/company/view">企業情報</a></li>';
    html += '            <li><a href="/user/">ユーザ管理</a></li>';
    html += '            <li><a href="#">その他</a></li>';
    html += '          </ul>';
    html += '        </li>';
    html += '      </ul>';
/*
    html += '      <form class="navbar-form navbar-right">';
    html += '        <input type="text" class="form-control" placeholder="Search...">';
    html += '      </form>';
*/
    html += '    </div>';

    document.write(html);
}