<html>
  <head>
    <title>広告主管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
    <style>
      div.center-block {
        width:200px;
      }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <!-- start container -->
    <div class="container-fluid">
      <h1 class="page-header">広告主管理</h1>

      <div class="form-horizontal">
        <div class="form-group">
          <label for="client_name_ja" class="col-xs-2 control-label">広告主名</label>
          <div class="col-xs-10">
            <input id="client_name_ja" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_name_en" class="col-xs-2 control-label">Client Name</label>
          <div class="col-xs-10">
            <input id="client_name_en" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_department" class="col-xs-2 control-label">広告主部署</label>
          <div class="col-xs-10">
            <div id="department_info">
              <table class="table">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>部署名</th>
                    <th>Department Name</th>
                    <th>ステータス</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>マーケティング部</td>
                    <td>Marketing Division</td>
                    <td><button type="button" class="btn btn-default department_status" value="0">未選択</button></td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>エージェント事業部</td>
                    <td>Agent Division</td>
                    <td><button type="button" class="btn btn-default department_status" value="0">未選択</button></td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>無敵事業部</td>
                    <td>God Division</td>
                    <td><button type="button" class="btn btn-default department_status" value="0">未選択</button></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="client_margin" class="col-xs-2 control-label">マージン率</label>
          <div class="col-xs-10">
            <input id="client_margin" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_country" class="col-xs-2 control-label">国名</label>
          <div class="col-xs-10">
            <input id="client_country" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_tel" class="col-xs-2 control-label">TEL</label>
          <div class="col-xs-10">
            <input id="client_tel" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_zipcode" class="col-xs-2 control-label">郵便番号</label>
          <div class="col-xs-10">
            <input id="client_zipcode" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_address_ja" class="col-xs-2 control-label">住所</label>
          <div class="col-xs-10">
            <input id="client_address_ja" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_address_en" class="col-xs-2 control-label">Address</label>
          <div class="col-xs-10">
            <input id="client_address_en" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_web" class="col-xs-2 control-label">HP</label>
          <div class="col-xs-10">
            <input id="client_web" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_note" class="col-xs-2 control-label">備考</label>
          <div class="col-xs-10">
            <input id="client_note" type="text" class="form-control"/>
          </div>
        </div>
      </div>
      <div class="center-block">
        <button type="button" class="btn btn-default" id="cancel">キャンセル</button>
        <button type="button" class="btn btn-success" id="submit">更新する</button>
      </div>
    </div>
    <?php echo $this->element('commonjs');?>
    <script>
    $(function(){
      $('#cancel, #submit').click(function(){
        location.href='view';
      });

      // 部署選択
      $('.department_status').click(function(){
        if($(this).val() == 0){
          $(this).val(1);
          $(this).removeClass('btn-default');
          $(this).addClass('btn-success');
          $(this).text('選択中');
        } else {
          $(this).val(0);
          $(this).removeClass('btn-success');
          $(this).addClass('btn-default');
          $(this).text('未選択');
        }
      });
    });
    </script>
  </body>
</html>