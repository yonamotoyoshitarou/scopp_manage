<html>
<head>
<?php echo $this->element('header');?>
<?php echo $this->Html->css('selectize/selectize.bootstrap2'); ?>
<?php echo $this->Html->css('selectize/selectize.bootstrap3'); ?>
<?php echo $this->Html->css('selectize/selectize'); ?>
<?php echo $this->Html->css('selectize/selectize.default'); ?>
<!-- dapepicker CSS -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <title>キャンペーンレポート　｜　SCOPP</title>
  <style>
    .search_condition{
      padding:0 8px !important;
    }
    .calendar {
      width:30% !important;
      display:inline-block !important;
    }
    .table th,td{
      vertical-align:middle !important;
    }
    #search_condition h1:hover{
      cursor:pointer;
    }
    .center-block{
      width:300px;
    }
  </style>
</head>
<body>
  <?php echo $this->element('nav');?>
  <div class="container-fluid">
      <form role="form" id="search_condition" action="./campaign.csv" method="get">
        <h1 class="page-header">
          <a href="javascript:void(0);"><span class="glyphicon glyphicon-chevron-up toggle"></span>
          <span class="glyphicon glyphicon-chevron-down toggle" style="display:none;"></span></a>
          キャンペーン検索<a class="pull-right" role="submit" id="csv"><span class="glyphicon glyphicon-download-alt"></span></a>
        </h1>

        <!-- start search -->
        <div id="target" class="search">
          <table class="table search_condition">
            <tbody>
              <tr>
                <th>広告主</th>
                <td>
                  <select class="demo-default selectized" name="camapaign_client" placeholder="Select a client..." autocomplete="off">
                    <option value=""><option>
                    <option value="1">apple</option>
                    <option value="2">cyber agent</option>
                    <option value="3">facebook</option>
                    <option value="4">google</option>
                  </select>
                  <select name="camapaign_client_department" placeholder="Select a department...">
                    <option value=""><option>
                    <option value="1">sales</option>
                    <option value="2">marketing</option>
                    <option value="3">media</option>
                    <option value="4">agent</option>
                  </select>
                </td>
                <th>代理店</th>
                <td>
                  <select class="demo-default selectized" name="camapaign_agent" placeholder="Select a agent..." autocomplete="off">
                    <option value=""><option>
                    <option value="1">apple</option>
                    <option value="2">cyber agent</option>
                    <option value="3">facebook</option>
                    <option value="4">google</option>
                  </select>
                  <select name="camapaign_agent_department" placeholder="Select a department...">
                    <option value=""><option>
                    <option value="1">sales</option>
                    <option value="2">marketing</option>
                    <option value="3">media</option>
                    <option value="4">agent</option>
                  </select>
                </td>
                <th>実施期間</th>
                <td>
                  From:　<input type="text" id="campaign_date_from" class="calendar form-control" name="campaign_date_from"><br/>
                  To:　　<input type="text" id="campaign_date_to" class="calendar form-control" name="campaign_date_to">
                </td>
              </tr>
              <tr>
                <th>プロジェクト</th>
                <td>
                  <select class="demo-default selectized" name="camapaign_project" placeholder="Select a project..." autocomplete="off">
                    <option value=""><option>
                    <option value="1">apple</option>
                    <option value="2">cyber agent</option>
                    <option value="3">facebook</option>
                    <option value="4">google</option>
                  </select>
                </td>
                <th>キャンペーン</th>
                <td>
                  <select class="demo-default selectized" name="camapaign" placeholder="Select a campaign..." autocomplete="off">
                    <option value=""><option>
                    <option value="1">apple</option>
                    <option value="2">cyber agent</option>
                    <option value="3">facebook</option>
                    <option value="4">google</option>
                  </select>
                </td>
                <th>アプリ/サイト</th>
                <td>
                  <select class="demo-default selectized" name="camapaign_title" placeholder="Select a title..." autocomplete="off">
                    <option value=""><option>
                    <option value="1">apple</option>
                    <option value="2">cyber agent</option>
                    <option value="3">facebook</option>
                    <option value="4">google</option>
                  </select>
                </td>
              </tr>
            </tbody>
          </table>
          <div class="center-block">
            <button type="button" id="all_show_button" class="btn btn-default">全件表示</button>
            <button type="reset" class="btn btn-warning">リセット</button>
            <button type="button" id="search_button" class="btn btn-primary">検索実行</button>
          </div>
        </div>
        <!-- end search -->
      </form>

          <h2 class="sub-header">レポート結果　※金額は全部Gross表記じゃないとマージンばれる</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>プロジェクト名</th>
                  <th>キャンペーン名</th>
                  <th>広告主名<br/>部署名</th>
                  <th>代理店名<br/>部署名</th>
                  <th>アプリ/サイト名</th>
                  <th>実施期間</th>
                  <th>imp数</th>
                  <th>click数</th>
                  <th>CTR</th>
                  <th>消化金額</th>
                  <th>CV数</th>
                  <th>CVR</th>
                  <th>CPA</th>
                  <th>平均CPC</th>
                  <th>入札単価</th>
                  <th>最高単価</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td><a href="../ticket/project/view.html">Project X</a></td>
                  <td><a href="../ticket/campaign/view.html">Campaign 1</a></td>
                  <td>株式会社AA<br/>マーケティング部</td>
                  <td>株式会社BB<br/>エージェント事業部</td>
                  <td>アプリA</td>
                  <td>2015/06/20 ~ 2015/07/20</td>
                  <td>10,000</td>
                  <td>200</td>
                  <td>2.0%</td>
                  <td>￥50,000</td>
                  <td>40</td>
                  <td>20%</td>
                  <td>￥1,250</td>
                  <td>￥250</td>
                  <td>￥5</td>
                  <td>￥10</td>
                </tr>
                <tr>
                  <td>1,002</td>
                  <td><a href="../ticket/project/view.html">Project X</a></td>
                  <td><a href="../ticket/campaign/view.html">Campaign 2</a></td>
                  <td>株式会社AA<br/>マーケティング部</td>
                  <td>株式会社BB<br/>エージェント事業部</td>
                  <td>アプリA</td>
                  <td>2015/06/20 ~ 2015/07/20</td>
                  <td>-</td>
                  <td>200</td>
                  <td>-%</td>
                  <td>￥-</td>
                  <td>-</td>
                  <td>-%</td>
                  <td>￥-</td>
                  <td>￥-</td>
                  <td>￥-</td>
                  <td>￥-</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>Integer</td>
                  <td>nec</td>
                  <td>odio</td>
                  <td>Praesent</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>libero</td>
                  <td>Sed</td>
                  <td>cursus</td>
                  <td>ante</td>
                </tr>
                <tr>
                  <td>1,004</td>
                  <td>dapibus</td>
                  <td>diam</td>
                  <td>Sed</td>
                  <td>nisi</td>
                </tr>
                <tr>
                  <td>1,005</td>
                  <td>Nulla</td>
                  <td>quis</td>
                  <td>sem</td>
                  <td>at</td>
                </tr>
                <tr>
                  <td>1,006</td>
                  <td>nibh</td>
                  <td>elementum</td>
                  <td>imperdiet</td>
                  <td>Duis</td>
                </tr>
                <tr>
                  <td>1,007</td>
                  <td>sagittis</td>
                  <td>ipsum</td>
                  <td>Praesent</td>
                  <td>mauris</td>
                </tr>
                <tr>
                  <td>1,008</td>
                  <td>Fusce</td>
                  <td>nec</td>
                  <td>tellus</td>
                  <td>sed</td>
                </tr>
                <tr>
                  <td>1,009</td>
                  <td>augue</td>
                  <td>semper</td>
                  <td>porta</td>
                  <td>Mauris</td>
                </tr>
                <tr>
                  <td>1,010</td>
                  <td>massa</td>
                  <td>Vestibulum</td>
                  <td>lacinia</td>
                  <td>arcu</td>
                </tr>
                <tr>
                  <td>1,011</td>
                  <td>eget</td>
                  <td>nulla</td>
                  <td>Class</td>
                  <td>aptent</td>
                </tr>
                <tr>
                  <td>1,012</td>
                  <td>taciti</td>
                  <td>sociosqu</td>
                  <td>ad</td>
                  <td>litora</td>
                </tr>
                <tr>
                  <td>1,013</td>
                  <td>torquent</td>
                  <td>per</td>
                  <td>conubia</td>
                  <td>nostra</td>
                </tr>
                <tr>
                  <td>1,014</td>
                  <td>per</td>
                  <td>inceptos</td>
                  <td>himenaeos</td>
                  <td>Curabitur</td>
                </tr>
                <tr>
                  <td>1,015</td>
                  <td>sodales</td>
                  <td>ligula</td>
                  <td>in</td>
                  <td>libero</td>
                </tr>
              </tbody>
            </table>
          </div>
<!--
        </div>
      </div>
-->
    </div>

<?php echo $this->element('commonjs');?>

    <!-- dapepicker -->
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<?php echo $this->element('selectize');?>
    <script>
    $(function(){
      $('#csv').click(function(){
        location.href="http://yij.nazo.cc/ad/report/campaign.csv";
      });

      //select
      $('select').selectize({
        create: true,
        sortField: 'text'
      });

      //calendar
      var date = new Date();
      var year = date.getFullYear();
      $.datepicker.setDefaults({
        closeText: '閉じる',
        prevText: '&#x3c;前月 ',
        nextText: ' 次月&#x3e;',
        currentText: '今日',
        monthNames: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
        monthNamesShort: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
        dayNames: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
        dayNamesShort: ['日','月','火','水','木','金','土'],
        dayNamesMin: ['日','月','火','水','木','金','土'],
        weekHeader: '週',
        dateFormat: 'yy/mm/dd',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: '年',
        minDate: new Date(year, -100 - 1, 1),
        maxDate: new Date(year + 100, 12 - 1, 31)
      });
      $('.calendar').datepicker();
      $('#campaign_date_from').datepicker('setDate','-1m');
      $('#campaign_date_to').datepicker('setDate','today');

      // search toggle
      var searchToggleFlg = true; //open
      $('#search_condition h1').on('click', function(){
        if(!searchToggleFlg){
          $('.glyphicon-chevron-down').hide();
          $('.glyphicon-chevron-up').show();
          searchToggleFlg = true;
        } else {
          $('.glyphicon-chevron-down').show();
          $('.glyphicon-chevron-up').hide();
          searchToggleFlg = false;
        }
        $('#target').slideToggle('fast');
      });

      // search exe
      var search_url = ''; //初期のJSON URL
      var request_url = search_url;
      var query = $('#search_condition').serialize();

      $('#search_button').click(function(event) {
        detailFlg = false;
        query = $('#search_condition').serialize();
        request_url = search_url + '?' + query;
        dataLoad();
//        event.preventDefault();
      });

      function dataLoad(){
        // query投げて、結果のJSONを取得（limit, offset）
      };
    });
    </script>
  </body>
</html>
</body>
</html>