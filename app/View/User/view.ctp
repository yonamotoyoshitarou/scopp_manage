<html>
  <head>
    <?php echo $this->element('header');?>
    <title>ユーザ情報　｜　SCOPP</title>

    <style>
     .table th{
        text-align:right;
        width:20%;
      }
    </style>
  </head>
  <body>
    <?php echo $this->element('nav');?>

    <div class="container-fluid">
      <h1 class="page-header">ユーザ管理<a href="regist" role="button" aria-expanded="false" class="pull-right"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h1>

      <div class="table-responsive">
        <table class="table">
          <tbody>
            <tr>
              <th>ID</th>
              <td>123</td>
            </tr>
            <tr>
              <th>Login ID</th>
              <td>ikutani</td>
            </tr>
            <tr>
              <th>ユーザ名</th>
              <td>ikutani yutaroさん</td>
            </tr>
            <tr>
              <th>Password</th>
              <td>********</td>
            </tr>
            <tr>
              <th>権限</th>
              <td>Client（ALL）</td>
            </tr>
            <tr>
              <th>備考</th>
              <td>noteだよ</td>
            </tr>
            <tr>
              <th>作成日時</th>
              <td>2015/06/18 22:39</td>
            </tr>
            <tr>
              <th>更新日時</th>
              <td>2015/06/19 23:40</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <?php echo $this->element('commonjs');?>
  </body>
</html>
