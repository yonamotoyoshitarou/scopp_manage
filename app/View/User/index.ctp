<html>
<head>
<?php echo $this->element('header');?>
    <title>ユーザ管理　｜　SCOPP</title>
</head>
<body>
  <?php echo $this->element('nav');?>
  <div class="container-fluid">
      <h1 class="page-header">ユーザ管理<a class="pull-right" role="button"><span class=" glyphicon glyphicon-download-alt"></span></a></h1>

          <p>ユーザの数とか表示する？検索もいるか</p>


          <h2 class="sub-header">ユーザ一覧<a href="./regist" role="button" aria-expanded="false" class="pull-right"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Login ID</th>
                  <th>ユーザ名</th>
                  <th>権限</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td><a href="view">ikutani</a></td>
                  <td>ikutani yutaroさん</td>
                  <td>Admin</td>
                </tr>
                <tr>
                  <td>1,002</td>
                  <td><a href="/user/view">yamazaki</a></td>
                  <td>yamazaki yoheiさん</td>
                  <td>Manager</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td><a href="view">yonamoto</a></td>
                  <td>yonamoto yoshitaroさん</td>
                  <td>Admin</td>
                </tr>
                <tr>
                  <td>1,004</td>
                  <td><a href="view">sogawa</a></td>
                  <td>sogawa takumiさん</td>
                  <td>Member</td>
                </tr>
                <tr>
                  <td>1,005</td>
                  <td><a href="view">shoji</a></td>
                  <td>shoji yunosukeさん</td>
                  <td>Client（ALL）</td>
                </tr>
                <tr>
                  <td>1,006</td>
                  <td><a href="view">hayashi</a></td>
                  <td>ryota hayashiさん</td>
                  <td>Client（asukabu）</td>
                </tr>
              </tbody>
            </table>
          </div>
    </div>
<?php echo $this->element('commonjs');?>
</body>
</html>
