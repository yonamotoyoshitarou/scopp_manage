<html>
  <head>
    <title>ユーザ情報　｜　SCOPP</title>
  <?php echo $this->element('header');?>
  <!-- selectize css -->
  <?php echo $this->Html->css('selectize/selectize.bootstrap2'); ?>
  <?php echo $this->Html->css('selectize/selectize.bootstrap3'); ?>
  <?php echo $this->Html->css('selectize/selectize'); ?>
  <?php echo $this->Html->css('selectize/selectize.default'); ?>
    <style>
      div.center-block {
        width:200px;
      }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <!-- start container -->
    <div class="container-fluid">
      <h1 class="page-header">ユーザ管理</h1>

      <!-- start form -->
      <form class="form-user" method="post" action="registExe">
      <!-- start input area -->
      <div class="form-horizontal">
        <div class="form-group">
          <label for="user_login" class="col-xs-2 control-label">Login ID</label>
          <div class="col-xs-10">
            <input id="user_login" type="text" class="form-control" required autofocus/>
          </div>
        </div>
        <div class="form-group">
          <label for="user_name" class="col-xs-2 control-label">ユーザ名</label>
          <div class="col-xs-10">
            <input id="user_name" type="text" class="form-control" required autocomplete="off"/>
          </div>
        </div>
        <div class="form-group">
          <label for="user_password" class="col-xs-2 control-label">Password</label>
          <div class="col-xs-10">
            <input id="user_password" type="password" class="form-control" required/>
          </div>
        </div>
        <div class="form-group">
          <label for="user_authority" class="col-xs-2 control-label">権限</label> 
          <div class="col-xs-10">
            <label class="radio-inline">
              <input type="radio" class="user_authority" name="user_authority" value="1"/> Administrator
            </label>
            <label class="radio-inline">
              <input type="radio" class="user_authority" name="user_authority" value="2" checked/> Manager
            </label>
            <label class="radio-inline">
              <input type="radio" class="user_authority" name="user_authority" value="3"/> Member
            </label>
            <label class="radio-inline">
              <input type="radio" class="user_authority" name="user_authority" value="4"/> Client
            </label>

            <!-- start client select -->
            <div id="user_client" style="display:none;">
              <select class="demo-default selectized" name="camapaign_client" id="campaign_client" placeholder="Select a client..." autocomplete="off">
                    <option value=""><option>
                    <option value="1">apple</option>
                    <option value="2">cyber agent</option>
                    <option value="3">facebook</option>
                    <option value="4">google</option>
              </select>
              <select class="demo-default selectized" name="camapaign_client_department" id="campaign_client_department" placeholder="Select a department.." autocomplete="off" disabled>
                    <option value=""><option>
                    <option value="0">ALL</option>
                    <option value="1">apple</option>
                    <option value="2">cyber agent</option>
                    <option value="3">facebook</option>
                    <option value="4">google</option>
              </select>
            </div>
            <!-- end client select -->
          </div>
        </div>
        <div class="form-group">
          <label for="user_note" class="col-xs-2 control-label">備考</label>
          <div class="col-xs-10">
            <input id="user_note" type="text" class="form-control"/>
          </div>
        </div>
      </div>
      <!-- end input area <--></-->
      <div class="center-block">
        <button type="button" class="btn btn-default" id="cancel">キャンセル</button>
        <button type="submit" class="btn btn-success" id="submit">更新する</button>
      </div>
      </form>
      <!-- end form -->
    </div>
    <!-- end container -->

    <?php echo $this->element('commonjs');?>
    <?php echo $this->element('selectize');?>
    <script>
    $(function(){
      // Link
      $('#cancel').click(function(){
        location.href='view';
      });

      $('select').selectize({
        sortField: 'text',
      });

      // selectize
      var departmentCheck = function(){
        // ajaxでサーバにdepartment問い合わせる

        // selectの選択肢に組み込み
        // 参考：https://github.com/brianreavis/selectize.js/blob/master/examples/api.html
        // defaultはALL
/*
        $('#campaign_client_department')[0].selectize.clearOptions();
        var arr = [
          {id:1, title: 'new1'},
          {id:2, title: 'new2'},
          {id:3, title: 'new3'}
        ]
*/
        // enable select of department
        $('#campaign_client_department')[0].selectize.enable();
      }
      $('select')[0].selectize.on('change', departmentCheck);

      // client setting
      $('.user_authority').click(function(){
        if($(this).val() == 4){
          $('#user_client').show();
        } else {
          $('#user_client').hide();
          $('select')[0].selectize.setValue(null);
        }
      });
    });
    </script>
  </body>
</html>