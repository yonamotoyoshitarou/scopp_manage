<html>
  <head>
    <title>代理店部署管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
    <style>
     .table th{
        text-align:right;
      }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <div class="container-fluid">
      <h1 class="page-header">代理店部署管理<a href="regist" role="button" aria-expanded="false" class="pull-right"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h1>

      <div class="table-responsive">
        <table class="table">
          <tbody>
            <tr>
              <th>ID</th>
              <td>123</td>
            </tr>
            <tr>
              <th>代理店名</th>
              <td>株式会社ABC</td>
            </tr>
            <tr>
              <th>部署名</th>
              <td>マーケティング部</td>
            </tr>
            <tr>
              <th>Department Name</th>
              <td>Marketing Division</td>
            </tr>
            <tr>
              <th>Country</th>
              <td>Japan</td>
            </tr>
            <tr>
              <th>TEL</th>
              <td>03-3333-3333</td>
            </tr>
            <tr>
              <th>郵便番号</th>
              <td>123-0000</td>
            </tr>
            <tr>
              <th>住所</th>
              <td>東京都新宿区西新宿6－16－7　パークフロント西新宿201</td>
            </tr>
            <tr>
              <th>Address</th>
              <td></td>
            </tr>
            <tr>
              <th>HP</th>
              <td>http://www.abc.com</td>
            </tr>
            <tr>
              <th>備考</th>
              <td>noteだよ</td>
            </tr>
            <tr>
              <th>作成日時</th>
              <td>2015/06/18 22:39</td>
            </tr>
            <tr>
              <th>更新日時</th>
              <td>2015/06/19 23:40</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <?php echo $this->element('commonjs');?>
    <script>
    $(function(){
      $('#list').click(function(){
        location.href='/agent-department/';
      });
      $('#regist').click(function(){
        location.href='/agent-department/regist';
      });
    });
    </script>
  </body>
</html>