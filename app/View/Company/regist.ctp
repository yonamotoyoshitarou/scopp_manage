<html>
<head>
<?php echo $this->element('header');?>
    <title>会社情報　｜　SCOPP</title>
    <style>
      div.center-block {
        width:200px;
      }
    </style>
</head>
<body>
  <?php echo $this->element('nav');?>
  <div class="container-fluid">
      <h1 class="page-header">会社情報（編集ページ来れるのはAdminだけかな）</h1>

      <div class="form-horizontal">
        <div class="form-group">
          <label for="company_name" class="col-xs-2 control-label">会社名</label>
          <div class="col-xs-10">
            <input id="company_name" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="company_name_en" class="col-xs-2 control-label">Company Name</label>
          <div class="col-xs-10">
            <input id="company_name_en" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="company_tel" class="col-xs-2 control-label">TEL</label>
          <div class="col-xs-10">
            <input id="company_tel" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="company_zipcode" class="col-xs-2 control-label">郵便番号</label>
          <div class="col-xs-10">
            <input id="company_zipcode" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="company_address_ja" class="col-xs-2 control-label">住所</label>
          <div class="col-xs-10">
            <input id="company_address_ja" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="company_address_en" class="col-xs-2 control-label">Address</label>
          <div class="col-xs-10">
            <input id="company_address_en" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="company_web" class="col-xs-2 control-label">HP</label>
          <div class="col-xs-10">
            <input id="company_web" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="company_note" class="col-xs-2 control-label">備考</label>
          <div class="col-xs-10">
            <input id="company_note" type="text" class="form-control"/>
          </div>
        </div>
      </div>
    </div>
    <div class="center-block">
      <button type="button" class="btn btn-default">キャンセル</button>
      <button type="button" class="btn btn-success">更新する</button>
    </div>
<?php echo $this->element('commonjs');?>
</body>
</html>
