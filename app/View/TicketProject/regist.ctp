<html>
  <head>
    <title>プロジェクト管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
  <!-- datepicker CSS -->
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
  <!-- selectize css -->
  <?php echo $this->Html->css('selectize/selectize.bootstrap2'); ?>
  <?php echo $this->Html->css('selectize/selectize.bootstrap3'); ?>
  <?php echo $this->Html->css('selectize/selectize'); ?>
  <?php echo $this->Html->css('selectize/selectize.default'); ?>
    <style>
      div.center-block {
        width:200px;
      }
      div#project_info th{
        width:20%;
      }
    </style>

  </head>
  <body>
  <?php echo $this->element('nav');?>

    <!-- start container -->
    <div class="container-fluid">
      <h1 class="page-header">プロジェクト管理</h1>

      <div class="form-horizontal">
        <div class="form-group">
          <label for="project_title_id" class="col-xs-2 control-label">アプリ/サイト</label>
          <div class="col-xs-10">
            <select class="demo-default selectized" name="project_title_id" id="project_title_id" placeholder="Select a title..." autocomplete="off">
              <option value=""><option>
              <option value="1">apple</option>
              <option value="2">cyber agent</option>
              <option value="3">facebook</option>
              <option value="4">google</option>
            </select>
            <div id="title_info">
              <table class="table">
                <tbody>
                  <tr>
                    <th>広告主名</th>
                    <td>株式会社ABC</td>
                  </tr>
                  <tr>
                    <th>部署名</th>
                    <td>株式会社ABC</td>
                  </tr>
                  <tr>
                    <th>Platform</th>
                    <td>iOS, Android, Web</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="project_agent_id" class="col-xs-2 control-label">代理店</label>
          <div class="col-xs-10">
            <select class="demo-default selectized" name="project_agent_id" id="project_agent_id" placeholder="Select a agent..." autocomplete="off">
              <option value=""><option>
              <option value="0">None<option>
              <option value="1">apple</option>
              <option value="2">cyber agent</option>
              <option value="3">facebook</option>
              <option value="4">google</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="project_agent_department_id" class="col-xs-2 control-label">代理店部署</label>
          <div class="col-xs-10">
            <select class="demo-default selectized" name="project_agent_department_id" id="project_agent_department_id" placeholder="Select a department..." autocomplete="off" disabled>
              <option value=""><option>
              <option value="1">apple</option>
              <option value="2">cyber agent</option>
              <option value="3">facebook</option>
              <option value="4">google</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="project_name" class="col-xs-2 control-label">プロジェクト名</label>
          <div class="col-xs-10">
            <input id="project_name" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="project_margin" class="col-xs-2 control-label">マージン率</label>
          <div class="col-xs-10">
            <input id="project_margin" type="text" class="form-control" />
          </div>
        </div>
        <div class="form-group">
          <label for="project_destination" class="col-xs-2 control-label">プロモーションURL</label>
          <div class="col-xs-10">
            <input id="project_destination" type="text" class="form-control" />
          </div>
        </div>
        <div class="form-group">
          <label for="project_note" class="col-xs-2 control-label">備考</label>
          <div class="col-xs-10">
            <textarea id="project_note" class="form-control" rows="4"/></textarea>
          </div>
        </div>
      </div>
      <div class="center-block">
        <button type="button" class="btn btn-default" id="cancel">キャンセル</button>
        <button type="button" class="btn btn-success" id="submit">更新する</button>
      </div>
    </div>
    <?php echo $this->element('commonjs');?>
    <?php echo $this->element('selectize');?>
    <script>
    $(function(){
      $('#cancel, #submit').click(function(){
        location.href='view';
      });

      // select
      $('select').selectize({
        sortField: 'value',
      });

      // when title has changed
      var titleCheck = function(){
        if($('#project_title_id').val() != 1){  // 正しくはproject_idの存在判定だけど、とりあえずデモ的に。
          $('#title_info').hide();

          var i = typeof($('#project_title_id').val());
          console.log(i);
        } else {
          // サーバから渡されてるidを使ってajaxでtitle_infoを取得する

          // 取得したデータをテーブルに組み込んで、title_infoを表示する


          $('#title_info').show();
          console.log('show');
        }
      }
      titleCheck();
      $('#project_title_id')[0].selectize.on('change', titleCheck);

      // when department has changed
      var departmentCheck = function(){
        // ajaxでサーバにdepartment問い合わせる

        // selectの選択肢に組み込み
        // 参考：https://github.com/brianreavis/selectize.js/blob/master/examples/api.html
        // defaultはALL
/*
        $('#campaign_client_department')[0].selectize.clearOptions();
        var arr = [
          {id:1, title: 'new1'},
          {id:2, title: 'new2'},
          {id:3, title: 'new3'}
        ]
*/
        // enable select of department
        $('#project_agent_department_id')[0].selectize.enable();
      }
      $('#project_agent_id')[0].selectize.on('change', departmentCheck);

    });
    </script>
  </body>
</html>