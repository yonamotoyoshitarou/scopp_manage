<html>
  <head>
    <title>プロジェクト管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
    <style>
     .table th{
        text-align:right;
        width:20%;
      }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <div class="container-fluid">
      <h1 class="page-header">プロジェクト管理<a href="regist" role="button" aria-expanded="false" class="pull-right"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h1>

      <div class="table-responsive">
        <table class="table">
          <tbody>
            <tr>
              <th>ID</th>
              <td>123</td>
            </tr>
            <tr>
              <th>広告主名</th>
              <td>株式会社ABC</td>
            </tr>
            <tr>
              <th>部署名</th>
              <td>マーケティング部</td>
            </tr>
            <tr>
              <th>アプリ/サイト名</th>
              <td>アプリA</td>
            </tr>
            <tr>
              <th>Platform</th>
              <td>iOS, Android, Web</td>
            </tr>
            <tr>
              <th>代理店名</th>
              <td>株式会社DEF</td>
            </tr>
            <tr>
              <th>部署名</th>
              <td>エージェント事業部</td>
            </tr>
            <tr>
              <th>プロジェクト名</th>
              <td>プロジェクトX</td>
            </tr>
            <tr>
              <th>マージン率</th>
              <td>15％</td>
            </tr>
            <tr>
              <th>プロモーションURL</th>
              <td>https://www.abc.jp</td>
            </tr>
            <tr>
              <th>備考</th>
              <td>noteだよ</td>
            </tr>
            <tr>
              <th>作成日時</th>
              <td>2015/06/18 22:39</td>
            </tr>
            <tr>
              <th>更新日時</th>
              <td>2015/06/19 23:40</td>
            </tr>
            <tr>
              <th>One Tag</th>
              <td>ボタン</td>
            </tr>
            <tr>
              <th>その他候補</th>
              <td>クライアントの総合予算（Gross表記）、期間、要望、縛り、ステータスなど</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
キャンペーンリストへのボタン、クリエイティブへのボタン・・？、レポートへのボタン
    <?php echo $this->element('commonjs');?>
    <script>
    $(function(){
      $('#list').click(function(){
        location.href='/master/creative/';
      });
    });
    </script>
  </body>
</html>