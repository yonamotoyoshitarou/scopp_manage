<html>
  <head>
    <title>キャンペーン管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
    <style>
      .table th{
        text-align:right;
        width:20%;
      }
      #creative_info th{
        text-align:center;
      }
      #creative_info td{
        text-align:center;
      }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <div class="container-fluid">
      <h1 class="page-header">キャンペーン管理<a href="./regist.html" role="button" aria-expanded="false" class="pull-right"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h1>

      <div class="table-responsive">
        <table class="table">
          <tbody>
            <tr>
              <th>ID</th>
              <td>123</td>
            </tr>
            <tr>
              <th>プロジェクト名</th>
              <td>プロジェクトX</td>
            </tr>
            <tr>
              <th>キャンペーン名</th>
              <td>キャンペーンX</td>
            </tr>
            <tr>
              <th>パートナー</th>
              <td>GREE DSP</td>
            </tr>
            <tr>
              <th>広告主名</th>
              <td>株式会社ABC</td>
            </tr>
            <tr>
              <th>部署名</th>
              <td>マーケティング部</td>
            </tr>
            <tr>
              <th>代理店名</th>
              <td>株式会社DEF</td>
            </tr>
            <tr>
              <th>部署名</th>
              <td>エージェント事業部</td>
            </tr>
            <tr>
              <th>アプリ/サイト名</th>
              <td>アプリA</td>
            </tr>
            <tr>
              <th>Platform</th>
              <td>iOS</td>
            </tr>
            <tr>
              <th>マージン率</th>
              <td>15％</td>
            </tr>
            <tr>
              <th>実施期間</th>
              <td>2015/06/15 ~ 2015/06/22</td>
            </tr>
            <tr>
              <th>予算</th>
              <td>500,000円　※Grossじゃないと俺らのマージンばれる</td>
            </tr>
            <tr>
              <th>要望詳細</th>
              <td>上限日予算とか。曜日ごとの違いとか。配信日指定とか。いろいろとメモしておく</td>
            </tr>
            <tr>
              <th>購入単位</th>
              <td>CPC</td>
            </tr>
            <tr>
              <th>Frequency cap</th>
              <td>同じ人に1日に何回まで表示していいか　※デフォルト値は指定なし</td>
            </tr>
            <tr>
              <th>カテゴリー</th>
              <td>？？</td>
            </tr>
            <tr>
              <th>ステータス</th>
              <td>配信中</td>
            </tr>
            <tr>
              <th>クリエイティブ</th>
              <td>
                <button type="button" class="btn btn-default" id="list"><span class="glyphicon glyphicon-list" aria-hidden="true"></span>　選択する</button>
                <div id="creative_info">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>画像</th>
                        <th>URL</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>画像ファイル表示</td>
                        <td>htttps://admin.scopp.com/?campaign_id=4&creative_id=4&redirect=abcdefg</td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>画像ファイル表示</td>
                        <td>htttps://admin.scopp.com/?campaign_id=4&creative_id=5&redirect=abcdefg</td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>画像ファイル表示</td>
                        <td>htttps://admin.scopp.com/?campaign_id=4&creative_id=6&redirect=abcdefg</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </td>
            </tr>
            <tr>
              <th>備考</th>
              <td>noteだよ</td>
            </tr>
            <tr>
              <th>作成日時</th>
              <td>2015/06/18 22:39</td>
            </tr>
            <tr>
              <th>更新日時</th>
              <td>2015/06/19 23:40</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <?php echo $this->element('commonjs');?>
    <script>
    $(function(){
      $('#list').click(function(){
        location.href='creative';
      });
    });
    </script>
  </body>
</html>