<html>
  <head>
    <title>キャンペーン管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
  <!-- selectize css -->
  <?php echo $this->Html->css('selectize/selectize.bootstrap2'); ?>
  <?php echo $this->Html->css('selectize/selectize.bootstrap3'); ?>
  <?php echo $this->Html->css('selectize/selectize'); ?>
  <?php echo $this->Html->css('selectize/selectize.default'); ?>
    <style>
    .table th,td{
      vertical-align:middle !important;
    }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <!-- start container -->
    <div class="container-fluid">
      <h1 class="page-header">キャンペーン管理<a class="pull-right" role="button"><span class=" glyphicon glyphicon-download-alt"></span></a></h1>

          <p>検索</p>


          <h2 class="sub-header">キャンペーン一覧<a href="regist" role="button" aria-expanded="false" class="pull-right"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>キャンペーン名</th>
                  <th>プロジェクト名</th>
                  <th>広告主名<br/>部署名</th>
                  <th>代理店名<br/>部署名</th>
                  <th>アプリ/サイト名</th>
                  <th>Platform</th>
                  <th>Partner</th>
                  <th>実施期間</th>
                  <th>ステータス</th>
                  <th><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td><a href="view">HIJKLMN</a></td>
                  <td><a href="/project/view">ABCDEFG</a></td>
                  <td>株式会社AA<br/>マーケティング部</td>
                  <td>株式会社BB<br/>エージェント事業部</td>
                  <td>アプリA</td>
                  <td>iOS</td>
                  <td>GREE DSP</td>
                  <td>2015/06/15 ~ 2015/06/22</td>
                  <td>配信中</td>
                  <td>
                    <a href="/report/campaign" class="btn btn-default"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>　レポーティング</a>
                  </td>
                </tr>
                <tr>
                  <td>1,002</td>
                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                  <td>elit</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>Integer</td>
                  <td>nec</td>
                  <td>odio</td>
                  <td>Praesent</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>libero</td>
                  <td>Sed</td>
                  <td>cursus</td>
                  <td>ante</td>
                </tr>
                <tr>
                  <td>1,004</td>
                  <td>dapibus</td>
                  <td>diam</td>
                  <td>Sed</td>
                  <td>nisi</td>
                </tr>
                <tr>
                  <td>1,005</td>
                  <td>Nulla</td>
                  <td>quis</td>
                  <td>sem</td>
                  <td>at</td>
                </tr>
                <tr>
                  <td>1,006</td>
                  <td>nibh</td>
                  <td>elementum</td>
                  <td>imperdiet</td>
                  <td>Duis</td>
                </tr>
                <tr>
                  <td>1,007</td>
                  <td>sagittis</td>
                  <td>ipsum</td>
                  <td>Praesent</td>
                  <td>mauris</td>
                </tr>
                <tr>
                  <td>1,008</td>
                  <td>Fusce</td>
                  <td>nec</td>
                  <td>tellus</td>
                  <td>sed</td>
                </tr>
                <tr>
                  <td>1,009</td>
                  <td>augue</td>
                  <td>semper</td>
                  <td>porta</td>
                  <td>Mauris</td>
                </tr>
                <tr>
                  <td>1,010</td>
                  <td>massa</td>
                  <td>Vestibulum</td>
                  <td>lacinia</td>
                  <td>arcu</td>
                </tr>
                <tr>
                  <td>1,011</td>
                  <td>eget</td>
                  <td>nulla</td>
                  <td>Class</td>
                  <td>aptent</td>
                </tr>
                <tr>
                  <td>1,012</td>
                  <td>taciti</td>
                  <td>sociosqu</td>
                  <td>ad</td>
                  <td>litora</td>
                </tr>
                <tr>
                  <td>1,013</td>
                  <td>torquent</td>
                  <td>per</td>
                  <td>conubia</td>
                  <td>nostra</td>
                </tr>
                <tr>
                  <td>1,014</td>
                  <td>per</td>
                  <td>inceptos</td>
                  <td>himenaeos</td>
                  <td>Curabitur</td>
                </tr>
                <tr>
                  <td>1,015</td>
                  <td>sodales</td>
                  <td>ligula</td>
                  <td>in</td>
                  <td>libero</td>
                </tr>
              </tbody>
            </table>
          </div>
    </div>

    <?php echo $this->element('commonjs');?>
    <?php echo $this->element('selectize');?>
  </body>
</html>