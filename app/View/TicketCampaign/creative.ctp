<html>
  <head>
    <title>代理店管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <div class="container-fluid">
      <h1 class="page-header">キャンペーン管理</h1>

          <p>キャンペーン情報の表示（広告主とか代理店とかアプリとか</p>


          <h2 class="sub-header">クリエイティブ選択<a href="view" role="button" aria-expanded="false" class="pull-right"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a></h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>広告主名</th>
                  <th>部署名</th>
                  <th>クリエイティブ</th>
                  <th>詳細</th>
                  <th>ステータス</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td><a href="/master/client/view">株式会社AA</a></td>
                  <td><a href="/master/client-department/view">マーケティング部</a></td>
                  <td>画像ファイル</td>
                  <td>
                    <a href="/master/creative/view" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>　表示</a>
                    <a href="/master/creative/regist" class="btn btn-danger"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>　編集</a>
                  </td>
                  <td><button type="button" class="btn btn-default creative_status" value="0">未選択</button></td>
                </tr>
                <tr>
                  <td>1,002</td>
                  <td><a href="/master/client/view">株式会社AA</a></td>
                  <td><a href="/master/client-department/view">マーケティング部</a></td>
                  <td>画像ファイル</td>
                  <td>
                    <a href="/master/creative/view" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>　表示</a>
                    <a href="/master/creative/regist" class="btn btn-danger"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>　編集</a>
                  </td>
                  <td><button type="button" class="btn btn-success creative_status" value="1">選択中</button></td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td><a href="/master/client/view">株式会社AA</a></td>
                  <td><a href="/master/client-department/view">マーケティング部</a></td>
                  <td>画像ファイル</td>
                  <td>
                    <a href="view" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>　表示</a>
                    <a href="regist" class="btn btn-danger"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>　編集</a>
                  </td>
                  <td><button type="button" class="btn btn-success creative_status" value="1">選択中</button></td>
                </tr>
                <tr>
                  <td>1,004</td>
                  <td><a href="/client/view">株式会社AA</a></td>
                  <td><a href="/client-department/view">マーケティング部</a></td>
                  <td>画像ファイル</td>
                  <td>
                    <a href="view" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>　表示</a>
                    <a href="regist" class="btn btn-danger"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>　編集</a>
                  </td>
                  <td><button type="button" class="btn btn-default creative_status" value="0">未選択</button></td>
                </tr>
              </tbody>
            </table>
          </div>
    </div>

    <?php echo $this->element('commonjs');?>
    <script>
    $(function(){
      $('.btn-danger').click(function(){
        if(!confirm("マスタを編集すると、他のキャンペーンに使われている情報にも影響を及ぼす可能性があります。よろしいですか？")){
          return false;
        }
      });

      // 部署選択
      $('.creative_status').click(function(){
        if($(this).val() == 0){
          $(this).val(1);
          $(this).removeClass('btn-default');
          $(this).addClass('btn-success');
          $(this).text('選択中');
        } else {
          $(this).val(0);
          $(this).removeClass('btn-success');
          $(this).addClass('btn-default');
          $(this).text('未選択');
        }
      });
    });
    </script>  </body>
</html>