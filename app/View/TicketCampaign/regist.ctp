<html>
  <head>
    <title>キャンペーン管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
  <!-- datepicker CSS -->
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
  <!-- selectize css -->
  <?php echo $this->Html->css('selectize/selectize.bootstrap2'); ?>
  <?php echo $this->Html->css('selectize/selectize.bootstrap3'); ?>
  <?php echo $this->Html->css('selectize/selectize'); ?>
  <?php echo $this->Html->css('selectize/selectize.default'); ?>
    <style>
      div.center-block {
        width:200px;
      }
      div#project_info th{
        width:20%;
      }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <!-- start container -->
    <div class="container-fluid">
      <h1 class="page-header">キャンペーン管理</h1>

      <div class="form-horizontal">
        <div class="form-group">
          <label for="campaign_project_id" class="col-xs-2 control-label">プロジェクト</label>
          <div class="col-xs-10">
            <select class="demo-default selectized" name="campaign_project_id" id="campaign_project_id" placeholder="Select a project..." autocomplete="off">
              <option value=""><option>
              <option value="1">apple</option>
              <option value="2">cyber agent</option>
              <option value="3">facebook</option>
              <option value="4">google</option>
            </select>
            <div id="project_info">
              <table class="table">
                <tbody>
                  <tr>
                    <th>広告主名</th>
                    <td>株式会社ABC</td>
                  </tr>
                  <tr>
                    <th>部署名</th>
                    <td>株式会社ABC</td>
                  </tr>
                  <tr>
                    <th>代理店名</th>
                    <td>株式会社DEF</td>
                  </tr>
                  <tr>
                    <th>部署名</th>
                    <td>エージェント事業部</td>
                  </tr>
                  <tr>
                    <th>アプリ/サイト名</th>
                    <td>アプリA</td>
                  </tr>
                  <tr>
                    <th>Platform</th>
                    <td>Web, Windows</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_name" class="col-xs-2 control-label">キャンペーン名</label>
          <div class="col-xs-10">
            <input id="campaign_name" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_partner_id" class="col-xs-2 control-label">パートナー</label>
          <div class="col-xs-10">
            <select class="demo-default selectized" name="campmaign_partner_id" id="campaign_partner_id" placeholder="Select a partner..." autocomplete="off">
              <option value=""><option>
              <option value="1">apple</option>
              <option value="2">cyber agent</option>
              <option value="3">facebook</option>
              <option value="4">google</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_platform" class="col-xs-2 control-label">Platform</label>
          <div class="col-xs-10">
            <label class="radio-inline">
              <input type="radio" name="campaign_platform" value="1" checked/>iOS
            </label>
            <label class="radio-inline">
              <input type="radio" name="campaign_platform" value="2"/>Android
            </label>
            <label class="radio-inline">
              <input type="radio" name="campaign_platform" value="3"/>Web
            </label>
            <label class="radio-inline">
              <input type="radio" name="campaign_platform" value="4"/>Android
            </label>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_margin" class="col-xs-2 control-label">マージン率</label>
          <div class="col-xs-10">
            <input id="campaign_margin" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_date_from" class="col-xs-2 control-label">実施開始日</label>
          <div class="col-xs-10">
            <input id="campaign_date_from" type="text" class="form-control calendar"/>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_date_to" class="col-xs-2 control-label">実施終了日</label>
          <div class="col-xs-10">
            <input id="campaign_date_to" type="text" class="form-control calendar"/>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_budget" class="col-xs-2 control-label">予算</label>
          <div class="col-xs-10">
            <input id="campaign_badget" type="text" class="form-control" placeholder="Gross"/>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_request" class="col-xs-2 control-label">要望詳細</label>
          <div class="col-xs-10">
            <textarea id="campaign_request" class="form-control" rows="5"/></textarea>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_kgi" class="col-xs-2 control-label">購入単位</label>
          <div class="col-xs-10">
            <label class="radio-inline">
              <input type="radio" name="campaign_platform" value="1" checked/>CPA
            </label>
            <label class="radio-inline">
              <input type="radio" name="campaign_platform" value="2"/>CPC
            </label>
            <label class="radio-inline">
              <input type="radio" name="campaign_platform" value="3"/>CPE（Engagementの中身はクライアントと握る。計測はSDK任せ）
            </label>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_frequency_upper" class="col-xs-2 control-label">Frequency上限/日</label>
          <div class="col-xs-10">
            <input id="campaign_frequency_upper" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_category" class="col-xs-2 control-label">カテゴリ</label>
          <div class="col-xs-10">
            <select class="demo-default selectized" name="campmaign_category" id="campaign_category" placeholder="Select a category..." autocomplete="off" multiple>
              <option value=""><option>
              <option value="1">sports</option>
              <option value="2">health</option>
              <option value="3">utility</option>
              <option value="4">game & entertainment</option>
              <option value="5">finance</option>
              <option value="6">ジャンル6</option>
              <option value="7">ジャンル7</option>
              <option value="8">ジャンル8</option>
              <option value="9">ジャンル9</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_status" class="col-xs-2 control-label">ステータス</label>
          <div class="col-xs-10">
            <label class="radio-inline">
              <input type="radio" name="campaign_platform" value="1" checked/>準備中
            </label>
            <label class="radio-inline">
              <input type="radio" name="campaign_platform" value="2"/>配信中
            </label>
            <label class="radio-inline">
              <input type="radio" name="campaign_platform" value="3"/>配信停止中
            </label>
            <label class="radio-inline">
              <input type="radio" name="campaign_platform" value="4"/>配信終了
            </label>
          </div>
        </div>
        <div class="form-group">
          <label for="campaign_note" class="col-xs-2 control-label">備考</label>
          <div class="col-xs-10">
            <textarea id="campaign_note" class="form-control" rows="2"/></textarea>
          </div>
        </div>
      </div>
      <div class="center-block">
        <button type="button" class="btn btn-default" id="cancel">キャンセル</button>
        <button type="button" class="btn btn-success" id="submit">更新する</button>
      </div>
    </div>
    <?php echo $this->element('commonjs');?>
    <?php echo $this->element('selectize');?>
    <!-- dapepicker -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script>
    $(function(){
      $('#cancel, #submit').click(function(){
        location.href='view';
      });

      //calendar
      var date = new Date();
      var year = date.getFullYear();
      $.datepicker.setDefaults({
        closeText: '閉じる',
        prevText: '&#x3c;前月 ',
        nextText: ' 次月&#x3e;',
        currentText: '今日',
        monthNames: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
        monthNamesShort: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
        dayNames: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
        dayNamesShort: ['日','月','火','水','木','金','土'],
        dayNamesMin: ['日','月','火','水','木','金','土'],
        weekHeader: '週',
        dateFormat: 'yy/mm/dd',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: '年',
        minDate: new Date(year, -100 - 1, 1),
        maxDate: new Date(year + 100, 12 - 1, 31)
      });
      $('.calendar').datepicker();

      // select
      $('select').selectize({
        sortField: 'value',
      });

      // when project has changed
      var projectCheck = function(){
        if($('#campaign_project_id').val() != 1){  // 正しくはtypeof使ったほうがいいけど、とりあえずデモ的に。
          $('#project_info').hide();

          var i = typeof($('#campaign_project_id').val());
          console.log(i);
        } else {
          // サーバから渡されてるidを使ってajaxでtitle_infoを取得する

          // 取得したデータをテーブルに組み込んで、title_infoを表示する


          $('#project_info').show();
          console.log('show');
        }
      }
      projectCheck();
      $('#campaign_project_id')[0].selectize.on('change', projectCheck);
    });
    </script>
  </body>
</html>