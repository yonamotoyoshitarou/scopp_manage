<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">SCOPP管理画面</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">マスタ<span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/master/title/">アプリ/サイト</a></li>
            <li><a href="/master/creative/">クリエイティブ</a></li>
            <li><a href="/master/client/">広告主</a></li>
            <li><a href="/master/client-department/">広告主部署</a></li>
            <li><a href="/master/agent/">代理店</a></li>
            <li><a href="/master/agent-department/">代理店部署</a></li>
            <li><a href="/master/partner/">パートナー</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">キャンペーン<span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/ticket/project/">プロジェクト</a></li>
            <li><a href="/ticket/campaign/">キャンペーン</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">レポート<span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/report/project">プロジェクト</a></li>
            <li><a href="/report/campaign">キャンペーン</a></li>
            <li><a href="#">外部提供データ</a></li>
            <li><a href="/report/bill">請求データ</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/company/view">企業情報</a></li>
            <li><a href="/user/">ユーザ管理</a></li>
            <li><a href="#">その他</a></li>
          </ul>
        </li>
      </ul>
    <!--
      <form class="navbar-form navbar-right">
        <input type="text" class="form-control" placeholder="Search...">
      </form>
    -->
    </div>
  </div>
</nav>
