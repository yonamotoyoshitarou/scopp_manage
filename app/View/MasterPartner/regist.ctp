<html>
  <head>
    <title>パートナー管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
    <style>
      div.center-block {
        width:200px;
      }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <!-- start container -->
    <div class="container-fluid">
      <h1 class="page-header">パートナー管理</h1>

      <div class="form-horizontal">
        <div class="form-group">
          <label for="partner_name_ja" class="col-xs-2 control-label">パートナー名</label>
          <div class="col-xs-10">
            <input id="partner_name_ja" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="partner_name_en" class="col-xs-2 control-label">Partner Name</label>
          <div class="col-xs-10">
            <input id="partner_name_en" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="partner_type" class="col-xs-2 control-label">タイプ</label>
          <div class="col-xs-10">
            <label class="radio-inline">
              <input type="radio" name="partner_type"/>DSP
            </label>
            <label class="radio-inline">
              <input type="radio" name="partner_type"/>ADNW
            </label>
            <label class="radio-inline">
              <input type="radio" name="partner_type"/>RWD
            </label>
            ※あとで増やす可能性あり
          </div>
        </div>
        <div class="form-group">
          <label for="partner_country" class="col-xs-2 control-label">Country</label>
          <div class="col-xs-10">
            <input id="partner_country" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="partner_tel" class="col-xs-2 control-label">TEL</label>
          <div class="col-xs-10">
            <input id="partner_tel" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="partner_zipcode" class="col-xs-2 control-label">郵便番号</label>
          <div class="col-xs-10">
            <input id="partner_zipcode" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="partner_address_ja" class="col-xs-2 control-label">住所</label>
          <div class="col-xs-10">
            <input id="partner_address_ja" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="partner_address_en" class="col-xs-2 control-label">Address</label>
          <div class="col-xs-10">
            <input id="partner_address_en" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="partner_web" class="col-xs-2 control-label">HP</label>
          <div class="col-xs-10">
            <input id="partner_web" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="partner_note" class="col-xs-2 control-label">備考</label>
          <div class="col-xs-10">
            <input id="partner_note" type="text" class="form-control"/>
          </div>
        </div>
      </div>
      <div class="center-block">
        <button type="button" class="btn btn-default" id="cancel">キャンセル</button>
        <button type="button" class="btn btn-success" id="submit">更新する</button>
      </div>
  </div>
      <?php echo $this->element('commonjs');?>
    <?php echo $this->element('selectize');?>
    <script>
    $(function(){
      $('#cancel, #submit').click(function(){
        location.href='view';
      });
    });
    </script>
  </body>
</html>