<html>
  <head>
    <title>広告主部署管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
  <!-- selectize css -->
  <?php echo $this->Html->css('selectize/selectize.bootstrap2'); ?>
  <?php echo $this->Html->css('selectize/selectize.bootstrap3'); ?>
  <?php echo $this->Html->css('selectize/selectize'); ?>
  <?php echo $this->Html->css('selectize/selectize.default'); ?>
    <style>
      div.center-block {
        width:350px;
      }
      .checkbox-inline {
        margin-left:1em;
        padding-left:0 !important;
      }
      input[type=checkbox]{
        margin-left:0 !important;
      }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <!-- start container -->
    <div class="container-fluid">
      <h1 class="page-header">広告主部署管理</h1>

      <div class="form-horizontal">
        <div class="form-group">
          <label for="client_department_client_id" class="col-xs-2 control-label">広告主名</label>
          <div class="col-xs-10">
            <select class="demo-default selectized" name="client_department_client_id" placeholder="Select a client..." autocomplete="off">
              <option value=""><option>
              <option value="1">apple</option>
              <option value="2">cyber agent</option>
              <option value="3">facebook</option>
              <option value="4">google</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="client_department_name_ja" class="col-xs-2 control-label">部署名</label>
          <div class="col-xs-10">
            <input id="client_department_name_ja" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_department_name_en" class="col-xs-2 control-label">Department Name</label>
          <div class="col-xs-10">
            <input id="client_department_name_en" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_department_margin" class="col-xs-2 control-label">マージン率</label>
          <div class="col-xs-10">
            <input id="client_department_margin" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_department_country" class="col-xs-2 control-label">国名</label>
          <div class="col-xs-10">
            <input id="client_department_country" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_department_tel" class="col-xs-2 control-label">TEL</label>
          <div class="col-xs-10">
            <input id="client_department_tel" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_department_zipcode" class="col-xs-2 control-label">郵便番号</label>
          <div class="col-xs-10">
            <input id="client_department_zipcode" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_department_address_ja" class="col-xs-2 control-label">住所</label>
          <div class="col-xs-10">
            <input id="client_department_address_ja" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_department_address_en" class="col-xs-2 control-label">Address</label>
          <div class="col-xs-10">
            <input id="client_department_address_en" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_department_web" class="col-xs-2 control-label">HP</label>
          <div class="col-xs-10">
            <input id="client_department_web" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="client_department_note" class="col-xs-2 control-label">備考</label>
          <div class="col-xs-10">
            <input id="client_department_note" type="text" class="form-control"/>
          </div>
        </div>
      </div>
      <div class="center-block">
        <button type="button" class="btn btn-default" id="cancel">キャンセル</button>
        <button type="button" class="btn btn-success" id="submit">更新する</button>
        <label class="checkbox-inline">
          <input type="checkbox" name="auto_regist" value="1" checked>　自動紐付け
        </label>
      </div>
    </div>
  

    <?php echo $this->element('commonjs');?>
    <?php echo $this->element('selectize');?>
    <script>
    $(function(){
      $('#cancel, #submit').click(function(){
        location.href='view';
      });

      //select
      $('select').selectize({
        sortField: 'text'
      });
    });
    </script>

  </body>
</html>