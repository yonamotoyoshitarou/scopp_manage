<html>
  <head>
    <title>アプリ/サイト情報　｜　SCOPP</title>
  <?php echo $this->element('header');?>
  <!-- selectize css -->
  <?php echo $this->Html->css('selectize/selectize.bootstrap2'); ?>
  <?php echo $this->Html->css('selectize/selectize.bootstrap3'); ?>
  <?php echo $this->Html->css('selectize/selectize'); ?>
  <?php echo $this->Html->css('selectize/selectize.default'); ?>
    <style>
      div.center-block {
        width:200px;
      }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <!-- start container -->
    <div class="container-fluid">
      <h1 class="page-header">アプリ/サイト管理</h1>

      <div class="form-horizontal">
        <div class="form-group">
          <label for="title_client_id" class="col-xs-2 control-label">広告主</label>
          <div class="col-xs-10">
            <select class="demo-default selectized" name="title_client_id" id="title_client_id" placeholder="Select a client..." autocomplete="off">
              <option value=""><option>
              <option value="1">apple</option>
              <option value="2">cyber agent</option>
              <option value="3">facebook</option>
              <option value="4">google</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="title_client_department_id" class="col-xs-2 control-label">部署</label>
          <div class="col-xs-10">
            <select class="demo-default selectized" name="title_client_department_id" id="title_client_department_id" placeholder="Select a department..." autocomplete="off" disabled>
              <option value=""><option>
              <option value="1">apple</option>
              <option value="2">cyber agent</option>
              <option value="3">facebook</option>
              <option value="4">google</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="title_name" class="col-xs-2 control-label">アプリ/サイト名</label>
          <div class="col-xs-10">
            <input id="title_name" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="title_platform" class="col-xs-2 control-label">Platform</label>
          <div class="col-xs-10">
            <label class="checkbox-inline">
              <input type="checkbox"/>iOS
            </label>
            <label class="checkbox-inline">
              <input type="checkbox"/>Android
            </label>
            <label class="checkbox-inline">
              <input type="checkbox"/>Web
            </label>
            <label class="checkbox-inline">
              <input type="checkbox"/>Windows
            </label>
          </div>
        </div>
        <div class="form-group">
          <label for="title_margin" class="col-xs-2 control-label">マージン率</label>
          <div class="col-xs-10">
            <input id="title_margin" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="title_note" class="col-xs-2 control-label">備考</label>
          <div class="col-xs-10">
            <input id="title_note" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="title_sdk" class="col-xs-2 control-label">導入済SDK</label>
          <div class="col-xs-10">
            <input id="title_sdk" type="text" class="form-control"/>
          </div>
        </div>
        <div class="form-group">
          <label for="title_reference" class="col-xs-2 control-label">参考URL</label>
          <div class="col-xs-10">
            <input id="title_reference" type="text" class="form-control"/>
          </div>
        </div>
      </div>
      <div class="center-block">
        <button type="button" class="btn btn-default" id="cancel">キャンセル</button>
        <button type="button" class="btn btn-success" id="submit">更新する</button>
      </div>
    </div>

    <?php echo $this->element('commonjs');?>
    <?php echo $this->element('selectize');?>
    <script>
    $(function(){
      $('#cancel, #submit').click(function(){
        location.href='view';
      });

      // select
      $('select').selectize({
        sortField: 'text',
      });

      // selectize
      var departmentCheck = function(){
        // ajaxでサーバにdepartment問い合わせる

        // selectの選択肢に組み込み
        // 参考：https://github.com/brianreavis/selectize.js/blob/master/examples/api.html
        // defaultはALL
/*
        $('#campaign_client_department')[0].selectize.clearOptions();
        var arr = [
          {id:1, title: 'new1'},
          {id:2, title: 'new2'},
          {id:3, title: 'new3'}
        ]
*/
        // enable select of department
        $('#title_client_department_id')[0].selectize.enable();
      }
      $('select')[0].selectize.on('change', departmentCheck);
    });
    </script>
  </body>
</html>