<html>
  <head>
    <title>アプリ/サイト情報　｜　SCOPP</title>
  <?php echo $this->element('header');?>
  <!-- selectize css -->
  <?php echo $this->Html->css('selectize/selectize.bootstrap2'); ?>
  <?php echo $this->Html->css('selectize/selectize.bootstrap3'); ?>
  <?php echo $this->Html->css('selectize/selectize'); ?>
  <?php echo $this->Html->css('selectize/selectize.default'); ?>
    <style>
     .table th{
        text-align:right;
        width:20%;
      }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <!-- start container -->
    <div class="container-fluid">
      <h1 class="page-header">アプリ/サイト管理<a href="./regist.html" role="button" aria-expanded="false" class="pull-right"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h1>

      <div class="table-responsive">
        <table class="table">
          <tbody>
            <tr>
              <th>ID</th>
              <td>123</td>
            </tr>
            <tr>
              <th>広告主名</th>
              <td>株式会社ABC</td>
            </tr>
            <tr>
              <th>部署名</th>
              <td>マーケティング部</td>
            </tr>
            <tr>
              <th>アプリ/サイト名</th>
              <td>アプリA</td>
            </tr>
            <tr>
              <th>Platform</th>
              <td>
                iOS<br/>
                Android<br/>
                Web<br/>
              </td>
            </tr>
            <tr>
              <th>マージン率</th>
              <td>10%</td>
            </tr>
            <tr>
              <th>備考</th>
              <td>noteだよ</td>
            </tr>
            <tr>
              <th>導入済SDK</th>
              <td>どんなSDKを入れているか（俺らと連携が必要）</td>
            </tr>
            <tr>
              <th>参考ページ</th>
              <td>http://www.abc.com</td>
            </tr>
            <tr>
              <th>作成日時</th>
              <td>2015/06/18 22:39</td>
            </tr>
            <tr>
              <th>更新日時</th>
              <td>2015/06/19 23:40</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <?php echo $this->element('commonjs');?>
    <?php echo $this->element('selectize');?>
  </body>
</html>