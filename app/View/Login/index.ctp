<html>
<head>
<?php echo $this->element('header');?>
<?php echo $this->Html->css('signin'); ?>
    <title>ログイン　｜　SCOPP</title>
    <style>
      h2 {
        text-align:center;
      }
      .footer{
        position:fixed;
        right:2em;
        bottom:0;
      }
      .footer p{
        text-align:center;
      }
    </style>
</head>
<body>

    <div class="container">

      <form role="form" data-toggle="validator" class="form-signin" method="post" action="/dashboard/">
        <h2 class="form-signin-heading">SCOPP 管理画面</h2>
        <label for="inputEmail" class="sr-only">Username</label>
        <input type="text" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>

        <div class="footer">
          <p>Copyrights &copy; yij, All rights reserved.</p>
        </div>
      </form>
    </div> <!-- /container -->
    <?php echo $this->element('commonjs');?>

</body>
</html>
