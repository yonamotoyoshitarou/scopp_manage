<html>
  <head>
    <title>クリエイティブ管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
  <!-- selectize css -->
  <?php echo $this->Html->css('selectize/selectize.bootstrap2'); ?>
  <?php echo $this->Html->css('selectize/selectize.bootstrap3'); ?>
  <?php echo $this->Html->css('selectize/selectize'); ?>
  <?php echo $this->Html->css('selectize/selectize.default'); ?>
    <style>
      div.center-block {
        width:200px;
      }
      #creative_resource {
        border:none;
      }
      .imgInput{
        padding:6px 12px;
      }
      .imgInput img{
        margin-top:1em;
      }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <!-- start container -->
    <div class="container-fluid">
      <h1 class="page-header">クリエイティブ管理</h1>

      <div class="form-horizontal">
        <div class="form-group">
          <label for="creative_client_id" class="col-xs-2 control-label">広告主</label>
          <div class="col-xs-10">
            <select class="demo-default selectized" name="creative_client_id" id="creative_client_id" placeholder="Select a client..." autocomplete="off">
              <option value=""><option>
              <option value="1">apple</option>
              <option value="2">cyber agent</option>
              <option value="3">facebook</option>
              <option value="4">google</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="creative_client_department_id" class="col-xs-2 control-label">部署</label>
          <div class="col-xs-10">
            <select class="demo-default selectized" name="creative_client_department_id" id="creative_client_department_id" placeholder="Select a department..." autocomplete="off" disabled>
              <option value=""><option>
              <option value="1">apple</option>
              <option value="2">cyber agent</option>
              <option value="3">facebook</option>
              <option value="4">google</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="creative_resource" class="col-xs-2 control-label">クリエイティブ</label>
          <div class="col-xs-10 imgInput">
            <input id="creative_resource" type="file" name="creative_resource"/>
            <img src="/img/noimage.png" alt="NO IMAGE" class="imgView">
          </div>
        </div>
        <div class="form-group">
          <label for="creative_status" class="col-xs-2 control-label">ステータス</label>
          <div class="col-xs-10">
            <label class="checkbox-inline">
              <input type="radio" name="creative_status" checked/>審査待ち
            </label>
            <label class="checkbox-inline">
              <input type="radio" name="creative_status"/>承認済み
            </label>
            <label class="checkbox-inline">
              <input type="radio" name="creative_status"/>終了
            </label>
          </div>
        </div>
        <div class="form-group">
          <label for="creative_note" class="col-xs-2 control-label">備考</label>
          <div class="col-xs-10">
            <input id="creative_note" type="text" class="form-control"/>
          </div>
        </div>
      </div>
    </div>
    <div class="center-block">
      <button type="button" class="btn btn-default" id="cancel">キャンセル</button>
      <button type="button" class="btn btn-success" id="submit">更新する</button>
    </div>

    <?php echo $this->element('commonjs');?>
    <?php echo $this->element('selectize');?>
    <script>
    $(function(){
      $('#cancel, #submit').click(function(){
        location.href='view';
      });

      // select
      $('select').selectize({
        sortField: 'text',
      });

      // selectize
      var departmentCheck = function(){
        // ajaxでサーバにdepartment問い合わせる

        // selectの選択肢に組み込み
        // 参考：https://github.com/brianreavis/selectize.js/blob/master/examples/api.html
        // defaultはALL
/*
        $('#campaign_client_department')[0].selectize.clearOptions();
        var arr = [
          {id:1, title: 'new1'},
          {id:2, title: 'new2'},
          {id:3, title: 'new3'}
        ]
*/
        // enable select of department
        $('#creative_client_department_id')[0].selectize.enable();
      }
      $('select')[0].selectize.on('change', departmentCheck);

      // preview of img-file
      // http://black-flag.net/jquery/20150217-5587.html
      var setFileInput = $('.imgInput'),
      setFileImg = $('.imgView');
   
      setFileInput.each(function(){
          var selfFile = $(this),
          selfInput = $(this).find('input[type=file]'),
          prevElm = selfFile.find(setFileImg),
          orgPass = prevElm.attr('src');
   
          selfInput.change(function(){
              var file = $(this).prop('files')[0],
              fileRdr = new FileReader();
   
              if (!this.files.length){
                  prevElm.attr('src', orgPass);
                  return;
              } else {
                  if (!file.type.match('image.*')){
                      prevElm.attr('src', orgPass);
                      return;
                  } else {
                      fileRdr.onload = function() {
                          prevElm.attr('src', fileRdr.result);
                      }
                      fileRdr.readAsDataURL(file);
                  }
              }
          });
      });
    });
    </script>
  </body>
</html>