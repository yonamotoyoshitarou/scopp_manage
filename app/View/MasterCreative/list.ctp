<html>
  <head>
    <title>クリエイティブ管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <div class="container-fluid">
      <h1 class="page-header">クリエイティブ管理<a class="pull-right" role="button"><span class="glyphicon glyphicon-download-alt"></span></a></h1>

          <p>クリエイティブのマスタ情報をテーブルで表示しとく</p>


          <h2 class="sub-header">キャンペーン一覧</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>プロジェクト名</th>
                  <th>キャンペーン名</th>
                  <th>広告主名<br/>部署名</th>
                  <th>代理店名<br/>部署名</th>
                  <th>アプリ/サイト名</th>
                  <th>Partner</th>
                  <th>実施期間</th>
                  <th>URL</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td><a href="/ticket/project/view">ProjectX</a></td>
                  <td><a href="/ticket/campaign/view">CampaignABC</a></td>
                  <td><a href="/client/view">株式会社AA</a></td>
                  <td><a href="/client-department/view">マーケティング部</a></td>
                  <td><a href="/title/view">アプリA</a></td>
                  <td><a href="/partner/view">GREE DSP</a></td>
                  <td>2015/06/23 ~ 2015/07/02</td>
                  <td>https://server.jp?creative_id=1&campaign_id=1</td>
                </tr>
                <tr>
                  <td>1,002</td>
                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                  <td>elit</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>Integer</td>
                  <td>nec</td>
                  <td>odio</td>
                  <td>Praesent</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>libero</td>
                  <td>Sed</td>
                  <td>cursus</td>
                  <td>ante</td>
                </tr>
                <tr>
                  <td>1,004</td>
                  <td>dapibus</td>
                  <td>diam</td>
                  <td>Sed</td>
                  <td>nisi</td>
                </tr>
                <tr>
                  <td>1,005</td>
                  <td>Nulla</td>
                  <td>quis</td>
                  <td>sem</td>
                  <td>at</td>
                </tr>
                <tr>
                  <td>1,006</td>
                  <td>nibh</td>
                  <td>elementum</td>
                  <td>imperdiet</td>
                  <td>Duis</td>
                </tr>
                <tr>
                  <td>1,007</td>
                  <td>sagittis</td>
                  <td>ipsum</td>
                  <td>Praesent</td>
                  <td>mauris</td>
                </tr>
                <tr>
                  <td>1,008</td>
                  <td>Fusce</td>
                  <td>nec</td>
                  <td>tellus</td>
                  <td>sed</td>
                </tr>
                <tr>
                  <td>1,009</td>
                  <td>augue</td>
                  <td>semper</td>
                  <td>porta</td>
                  <td>Mauris</td>
                </tr>
                <tr>
                  <td>1,010</td>
                  <td>massa</td>
                  <td>Vestibulum</td>
                  <td>lacinia</td>
                  <td>arcu</td>
                </tr>
                <tr>
                  <td>1,011</td>
                  <td>eget</td>
                  <td>nulla</td>
                  <td>Class</td>
                  <td>aptent</td>
                </tr>
                <tr>
                  <td>1,012</td>
                  <td>taciti</td>
                  <td>sociosqu</td>
                  <td>ad</td>
                  <td>litora</td>
                </tr>
                <tr>
                  <td>1,013</td>
                  <td>torquent</td>
                  <td>per</td>
                  <td>conubia</td>
                  <td>nostra</td>
                </tr>
                <tr>
                  <td>1,014</td>
                  <td>per</td>
                  <td>inceptos</td>
                  <td>himenaeos</td>
                  <td>Curabitur</td>
                </tr>
                <tr>
                  <td>1,015</td>
                  <td>sodales</td>
                  <td>ligula</td>
                  <td>in</td>
                  <td>libero</td>
                </tr>
              </tbody>
            </table>
          </div>
    </div>
    <?php echo $this->element('commonjs');?>
    <?php echo $this->element('selectize');?>
  </body>
</html>