<html>
  <head>
    <title>クリエイティブ管理　｜　SCOPP</title>
  <?php echo $this->element('header');?>
    <style>
     .table th{
        text-align:right;
        width:20%;
      }
      img {
        margin-bottom:1em;
      }
    </style>
  </head>
  <body>
  <?php echo $this->element('nav');?>

    <div class="container-fluid">
      <h1 class="page-header">クリエイティブ管理<a href="regist" role="button" aria-expanded="false" class="pull-right"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h1>

      <div class="table-responsive">
        <table class="table">
          <tbody>
            <tr>
              <th>ID</th>
              <td>123</td>
            </tr>
            <tr>
              <th>広告主名</th>
              <td>株式会社ABC</td>
            </tr>
            <tr>
              <th>部署名</th>
              <td>マーケティング部</td>
            </tr>
            <tr>
              <th>クリエイティブ</th>
              <td>
                <img src="/img/noimage.png" alt="NO IMAGE"><br/>
                <a href="/ticket/campaign/" class="btn btn-default"><span class="glyphicon glyphicon-list" aria-hidden="true"></span>　キャンペーン一覧</a>
              </td>
            </tr>
            <tr>
              <th>ステータス</th>
              <td>審査待ち</td>
            </tr>
            <tr>
              <th>備考</th>
              <td>noteだよ</td>
            </tr>
            <tr>
              <th>作成日時</th>
              <td>2015/06/18 22:39</td>
            </tr>
            <tr>
              <th>更新日時</th>
              <td>2015/06/19 23:40</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <?php echo $this->element('commonjs');?>
  </body>
</html>